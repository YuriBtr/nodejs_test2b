import express from 'express';
import fetch from 'isomorphic-fetch';
import Promise from 'bluebird';
import _ from 'lodash';

const __DEV__ = true;

const app = express();
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

function checkName(name) {
  const regexp = new RegExp('[\\d\\-\\_\\+\\=\\\\/\\|\\[\\]\\(\\)\\*\\&\\^\\%\\$\\#\\@\\!\\~\\"\\?\\<\\>\\,\\.\\:\\;]', 'g');
  const matchArray = name.match(regexp);
  return !matchArray;
}

function toStartCase(str) {
  var res = '';
  for (var i = 0; i < str.length; i++) {
    if (i == 0) res = str[i].toUpperCase()
    else res = res + str[i].toLowerCase();
  }
  return res;
}

app.get('/task2B', (req, res, next) => {
  let fullName = req.query.fullname;
  console.log('Entered: ' + fullName);
  fullName = fullName.replace(/\s{2,}/g, ' ').trim();
  if (!fullName || !checkName(fullName)) {
    console.log('Pattern wrong');
    res.send('Invalid fullname');
    return;
  }
  const arrayFullName = _.split(fullName, ' ');
  for (var i = 0; i < arrayFullName.length; i++) {
    arrayFullName[i] = toStartCase(arrayFullName[i]);
  }

  console.log('Adopted: ' + arrayFullName);

  switch (arrayFullName.length) {
    case (3):
      console.log(arrayFullName[2] + ' ' + arrayFullName[0][0] + '. ' + arrayFullName[1][0] + '.');
      res.send(arrayFullName[2] + ' ' + arrayFullName[0][0] + '. ' + arrayFullName[1][0] + '.');
      break;
    case (2):
      console.log(arrayFullName[1] + ' ' + arrayFullName[0][0] + '.');
      res.send(arrayFullName[1] + ' ' + arrayFullName[0][0] + '.');
      break;
    case (1):
      console.log(arrayFullName[0]);
      res.send(arrayFullName[0]);
      break;
    default:
      console.log('Invalid fullname');
      res.send('Invalid fullname');
  }
  return;
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
